import pandas as pd
import numpy as np
from navipy.processing.mcode import optic_flow_rotationonal, optic_flow_translational
from navipy.maths.coordinates import cartesian_to_spherical
from navipy.trajectories import Trajectory
from functools import partial
from multiprocessing import Pool

def load_data(signaltype,
              head_filename_result,
             head_filename_result_blocknonans,
             head_filename_simulated_motion,
             beeid,blk_i):
    key = '/blk_nonans/flight_{}'.format(beeid) 
    blknonans = pd.read_hdf(head_filename_result_blocknonans,key)

    head_traj = dict()

    key = '/trajectories_{}/flight_{}/blk_{}'.format(signaltype,beeid,blk_i) 
    head_traj['signal'] = pd.read_hdf(head_filename_simulated_motion,key)
    # Convert into a navipy trajectory
    head_traj['signal'] = Trajectory().from_dataframe(head_traj['signal'])
    # 
    key = '/trajectories/flight_{}/blk_{}'.format(beeid,blk_i) 
    head_traj['measure'] = pd.read_hdf(head_filename_result_blocknonans,key).loc[:,'head']
    # Convert into a navipy trajectory
    head_traj['measure'] = Trajectory().from_dataframe(head_traj['measure'])
    #
    key = '/res_sac/flight_{}/blk_{}'.format(beeid,blk_i) 
    res_sac_head = pd.read_hdf(head_filename_result_blocknonans,key).loc[:,'head']

    head_traj['measure'].head()
    return head_traj, res_sac_head

def calculate_vel(head_traj):
    return head_traj.velocity(frame='body')


def calculate_posvel(head_traj):
    velocities = calculate_vel(head_traj)
    posvel = pd.concat([velocities, head_traj], axis=1)
    return posvel


def filter_unrealistic_speed(posvel, fps=500):
    '''remove the unrealistic speed i.e the tracking error'''
    maxspeed = 1  # m/s
    maxspeed = maxspeed*1000  # mm/s
    maxspeed = maxspeed/fps  # mm/frame

    speed = np.sum(posvel.location.loc[:, ['dx', 'dy', 'dz']]**2, axis=1)
    speed = np.sqrt(speed)
    print(speed[speed > maxspeed])

    posvel.loc[speed > maxspeed, :] = np.nan
    return posvel


def get_pointsonretina(head_traj, arena_points):
    '''from world cooridnate (cartesian) to retina coordinate (spherical)'''
    points2bee = head_traj.world2body(arena_points)
    tmp = points2bee.swaplevel(axis=1)

    el, az, radius = cartesian_to_spherical(tmp.x,
                                            tmp.y,
                                            tmp.z)

    points2bee_sh = pd.DataFrame(index=points2bee.index,
                                 columns=points2bee.columns)
    d = dict(zip(['x', 'y', 'z'], ['elevation', 'azimuth', 'radius']))
    points2bee_sh = points2bee_sh.rename(columns=d, level=1)
    points2bee_sh = points2bee_sh.swaplevel(axis=1)
    points2bee_sh.elevation = el
    points2bee_sh.azimuth = az
    points2bee_sh.radius = radius
    points2bee_sh = points2bee_sh.swaplevel(axis=1)
    return points2bee_sh


def calc_of_single(frame_i, posvel, points2bee_sh, sorted_poi):
    '''Calculating the optic flow witht the different'''
    cof = pd.Series(index=pd.MultiIndex.from_product([['rotational', 'translation'],
                                                      ['h', 'v'],
                                                      sorted_poi]))
    cof.name = frame_i
    curr_vel = posvel.loc[frame_i, :]
    # Position on retina
    cp2beesh = points2bee_sh.swaplevel(axis=1).loc[frame_i, :]
    viewing_directions = np.zeros((len(sorted_poi), 2))
    viewing_directions[..., 0] = cp2beesh.elevation.loc[sorted_poi]
    viewing_directions[..., 1] = cp2beesh.azimuth.loc[sorted_poi]
    # distance to retina
    distance = cp2beesh.radius.loc[sorted_poi].values
    # Optic flow
    _, hof_rot, vof_rot = optic_flow_rotationonal(viewing_directions, curr_vel)
    _, hof_trans, vof_trans = optic_flow_translational(
        distance, viewing_directions, curr_vel)
    for ii, ccol in enumerate(sorted_poi):
        cof.loc[('rotational', 'h', ccol)] = hof_rot[ii]
        cof.loc[('rotational', 'v', ccol)] = vof_rot[ii]
        cof.loc[('translation', 'h', ccol)] = hof_trans[ii]
        cof.loc[('translation', 'v', ccol)] = vof_trans[ii]
    return cof


def calc_of(posvel, points2bee_sh, p=None):
    # To guarantee that OF are not swapped
    sorted_poi = np.unique(points2bee_sh.columns.get_level_values(0))
    _calc_of = partial(calc_of_single, posvel=posvel,
                       points2bee_sh=points2bee_sh, sorted_poi=sorted_poi)
    close_p = False
    if p is None:
        close_p = True
        p = Pool()
    out = p.map(_calc_of, posvel.dropna().index)
    if len(out)<=0:
        optic_flow = None
    else:
        # Swap axis
        optic_flow = pd.concat([o for o in out], keys=[o.name for o in out])
        optic_flow = optic_flow.swaplevel(i=0, j=3).unstack().transpose()
        optic_flow.columns.names = ['Poi', 'Component', 'Axis']
        optic_flow.index.name = 'frame_i'
    if close_p:
        p.close()
    return optic_flow


def of_reindex_sacintersac(optic_flow,res_sac_head):
    # Average of during intersaccade and saccade
    optic_flow_saccade=dict()
    for name in optic_flow.keys():
        of_saccade = optic_flow[name].reindex(res_sac_head.saccade.dropna().index)
        of_saccade['frame_i'] = of_saccade.index
        of_saccade['sac_i'] = res_sac_head.saccade.dropna()
        of_saccade = of_saccade.set_index(['frame_i','sac_i'])
        # Add Trans + Rot
        of_saccade = of_saccade.xs('rotational',axis=1,level='Component') + \
                     of_saccade.xs('translation',axis=1,level='Component')
        optic_flow_saccade[name] = of_saccade.copy()

    optic_flow_intersac=dict()
    for name in optic_flow.keys():
        of_saccade = optic_flow[name].reindex(res_sac_head.intersac.dropna().index)
        of_saccade['frame_i'] = of_saccade.index
        of_saccade['intersac_i'] = res_sac_head.intersac.dropna()
        of_saccade = of_saccade.set_index(['frame_i','intersac_i'])
        # Add Trans + Rot
        of_saccade = of_saccade.xs('rotational',axis=1,level='Component') + \
                     of_saccade.xs('translation',axis=1,level='Component')
        optic_flow_intersac[name] = of_saccade.copy()
    return optic_flow_intersac, optic_flow_saccade



def get_signal2noise_ratio_signaltype(signaltype, 
                                      head_filename_result, 
                                      head_filename_result_blocknonans, 
                                      head_filename_simulated_motion,
                                      beeid, blk_i, arena_points, p=None):
    '''function to calulate the SNR of each intersaccade with a simulated trajectory'''
    # Load data:
    head_traj, res_sac_head = load_data(signaltype,
             head_filename_result,
             head_filename_result_blocknonans,
             head_filename_simulated_motion,
             beeid,blk_i)
    
    # Filter traj
    lamb = pd.Series(index=head_traj['measure'].columns, data=150)
    for key in head_traj.keys():
        head_traj[key].filt_cspline(lamb)
    
    # Calculate velocity
    posvel = {name:calculate_posvel(ht) for name, ht in head_traj.items()}
    
    # Filter velocity
    posvel = {name:filter_unrealistic_speed(ht) for name, ht in posvel.items()}
    
    # Project poi on bee retina
    points2bee_sh = {name:get_pointsonretina(ht, arena_points) for name, ht in head_traj.items()}

    # Calc optic flow
    optic_flow=dict()
    for name in head_traj.keys():
        optic_flow[name] = calc_of(posvel[name],points2bee_sh[name])
        if optic_flow[name] is None:
             return None

    # Reindex on intersaccade
    optic_flow_intersac, optic_flow_saccade = of_reindex_sacintersac(optic_flow,res_sac_head)    

    # -- Averages
    if optic_flow_intersac['measure'].shape[0] > 0:
        of_intersac_mean = {name:of.groupby('intersac_i').apply(np.mean) for name,of in optic_flow_intersac.items()}
    else:
        return None
    if optic_flow_saccade['measure'].shape[0] > 0:
        of_saccade_mean = {name:of.groupby('sac_i').apply(np.mean) for name,of in optic_flow_saccade.items()}
    else:
        return None

    # Calculate norm
    of_intersac_norm = {name:of.unstack().groupby(['Poi','intersac_i']).apply(np.linalg.norm) \
                        for name, of in of_intersac_mean.items()}
    of_saccade_norm = {name:of.unstack().groupby(['Poi','sac_i']).apply(np.linalg.norm) \
                        for name, of in of_saccade_mean.items()}

    # Signal to noise ratio
    noise = ((of_intersac_norm['measure']-of_intersac_norm['signal']).abs())
    snr_ofnorms_intsac = of_intersac_norm['signal']/noise

    # concat
    snr = pd.concat([snr_ofnorms_intsac.unstack().transpose()],axis=1,keys=['intersac'])
    # add feautres
    #features_all = get_features(points2bee_sh,posvel,error,res_sac_head)
    return snr
