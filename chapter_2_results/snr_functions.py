import pandas as pd
import numpy as np
from navipy.processing.mcode import optic_flow
from navipy.maths.coordinates import cartesian_to_spherical
from navipy.trajectories import Trajectory
from tqdm.notebook import tqdm


def load_data(signaltype,
              head_filename_result,
             head_filename_result_blocknonans,
             head_filename_simulated_motion,
             beeid,blk_i):
    key = '/blk_nonans/flight_{}'.format(beeid) 
    blknonans = pd.read_hdf(head_filename_result_blocknonans,key)

    head_traj = dict()

    key = '/trajectories_{}/flight_{}/blk_{}'.format(signaltype,beeid,blk_i) 
    head_traj['signal'] = pd.read_hdf(head_filename_simulated_motion,key)
    # Convert into a navipy trajectory
    head_traj['signal'] = Trajectory().from_dataframe(head_traj['signal'])
    # 
    key = '/trajectories/flight_{}/blk_{}'.format(beeid,blk_i) 
    head_traj['measure'] = pd.read_hdf(head_filename_result_blocknonans,key).loc[:,'head']
    # Convert into a navipy trajectory
    head_traj['measure'] = Trajectory().from_dataframe(head_traj['measure'])
    #
    key = '/res_sac/flight_{}/blk_{}'.format(beeid,blk_i) 
    res_sac_head = pd.read_hdf(head_filename_result_blocknonans,key).loc[:,'head']

    head_traj['measure'].head()
    return head_traj, res_sac_head


def calc_of(velocity, points_sh):
    pois = np.unique(points_sh.columns.get_level_values(level=0))
    optical_flow = dict()
    for frame_i, vel in tqdm(velocity.dropna().iterrows(), total=velocity.dropna().shape[0]):
        # cpoint is the projection of the points at frame_i
        # and arrange as table:
        #             next, exit
        # elevation
        # azimuth
        # radius
        cpoint = points_sh.swaplevel(axis=1).loc[frame_i,:].unstack()
        # To make sure they are correctly ordered 
        # in the output of optic_flow
        pois = cpoint.columns 
        distance = cpoint.loc['radius',:].values
        viewing_direction = cpoint.loc[['elevation','azimuth']].values
        out = optic_flow(distance, viewing_direction, vel)
        # Reformat output for filling datafra,e
        out = pd.DataFrame(out, index=['rof','hof','vof'], columns=pois)
        out = out.unstack()
        out.name = frame_i
        # Fill dataframe
        optical_flow[frame_i] = out.copy()
    optical_flow = pd.concat(optical_flow).unstack(level=0).transpose()
    return optical_flow.reindex(velocity.index)


def project_points(head_traj,arena_points):
    points2bee_ca = {name:ht.world2body(arena_points) for name, ht in head_traj.items()}
    points2bee_sh = dict()
    for name, pb in points2bee_ca.items():
        data = pb.swaplevel(axis=1)
        el,az,r = cartesian_to_spherical(data.x,data.y,data.z)
        data_sh = pd.concat([el,az,r],axis=1,keys=['elevation','azimuth','radius'])
        data_sh = data_sh.swaplevel(axis=1)
        points2bee_sh[name] = data_sh.copy()
    return points2bee_sh


def of_reindex_sacintersac(optical_flow,res_sac_head):
    # We will store the result in a new variable
    optic_flow_saccade=dict()
    # It is a dictionary because we have 
    # signal and measure to process
    for name in optical_flow.keys():
        of = optical_flow[name].unstack().transpose()
        # We process only the saccade, so we reindex
        # as the res_sac_head.saccade.dropna()
        of_saccade = of.reindex(res_sac_head.saccade.dropna().index)
        # We want to keep track of frame i and sac i for later comparison
        of_saccade['frame_i'] = of_saccade.index
        of_saccade['sac_i'] = res_sac_head.saccade.dropna()
        # We set as a multiindex
        of_saccade = of_saccade.set_index(['frame_i','sac_i'])
        # Add Trans + Rot
        #of_saccade = of_saccade.xs('rotational',axis=1,level='Component') + \
        #             of_saccade.xs('translation',axis=1,level='Component')
        # We can now assign the optic flow during saccade.
        optic_flow_saccade[name] = of_saccade.copy()

    # Same as above but during intersaccade.
    # We index on res_sac_head.intersac instead of saccade
    optic_flow_intersac=dict()
    for name in optical_flow.keys():
        of = optical_flow[name].unstack().transpose()
        of_intersaccade = of.reindex(res_sac_head.intersac.dropna().index)
        of_intersaccade['frame_i'] = of_intersaccade.index
        of_intersaccade['intersac_i'] = res_sac_head.intersac.dropna()
        of_intersaccade = of_intersaccade.set_index(['frame_i','intersac_i'])
        optic_flow_intersac[name] = of_intersaccade.copy()
    return optic_flow_intersac, optic_flow_saccade

def get_signal2noise_ratio_signaltype(signaltype, 
                                     head_filename_result,
                                     head_filename_result_blocknonans,
                                     head_filename_simulated_motion,
                                     beeid,blk_i,arena_points):
    
    # Load data:
    head_traj, res_sac_head = load_data(signaltype,
             head_filename_result,
             head_filename_result_blocknonans,
             head_filename_simulated_motion,
             beeid,blk_i)
    # 2) Filter
    lamb = pd.Series(index=head_traj['measure'].columns, data=150)
    for key in head_traj.keys():
        head_traj[key].filt_cspline(lamb)
    # 3) calvelocity
    posvel = {name:ht.velocity() for name, ht in head_traj.items()}
    # Project point to retina
    points2bee_sh = project_points(head_traj,arena_points)
    # 4) Calc optic flow
    optical_flow=dict()
    for name in head_traj.keys():
        optical_flow[name] =  calc_of(posvel[name],points2bee_sh[name])
        optical_flow[name] = optical_flow[name].unstack()
        optical_flow[name].index.names = ['Poi','Component','frame_i']
    # 5) Average along intersaccade
    optic_flow_intersac,_ = of_reindex_sacintersac(optical_flow,res_sac_head)
    # The noise is simply the measure minus the signal
    optic_flow_intersac['noise'] = optic_flow_intersac['measure'] - optic_flow_intersac['signal']
    # Average
    of_intersac_mean = {name:of.groupby('intersac_i').apply(np.mean) for name,of in optic_flow_intersac.items()}
    if of_intersac_mean['measure'].shape[0]==0:
        emptysnr = pd.Series(index=pd.MultiIndex.from_product(
            [np.unique(arena_points.index.get_level_values(0)),[]]),dtype=float)
        return emptysnr
    # Norm
    of_intersac_norm = {name:of.unstack().groupby(['Poi','intersac_i']).apply(np.linalg.norm) for name, of in of_intersac_mean.items()}
    snr = of_intersac_norm['signal']/of_intersac_norm['noise']
    return snr