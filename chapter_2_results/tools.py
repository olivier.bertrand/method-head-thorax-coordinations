import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style("white")
import numpy as np 
import pandas as pd 
from matplotlib.ticker import FuncFormatter
import matplotlib.transforms as mtransforms
from matplotlib import gridspec


def Do_df_from_hdf(path,key,part,flight_ids):
    '''later put in tool'''
    data={}
    
    for flight_id in flight_ids:

        key_1 = '/blk_nonans/flight_{}'.format(flight_id)
        # Load block no nans for current bee
        blknonans = pd.read_hdf(path, key_1)

        block_ids=blknonans.index

        data[flight_id]={}
        small_dic={}
        
        
        for blk_id in block_ids:
            res = pd.read_hdf(path,key=key.format(flight_id,blk_id)).loc[:,part]
            small_dic[blk_id]=res
            
        data[flight_id]=pd.concat(small_dic)
       
    DF_data=pd.concat(data)
    
    DF_data.index.names = ['flight_i','blk_i','frame_i']
    
    
    return DF_data


def plot_traj_orient(DATA,ERROR,VEL,RES,sub,fontsize,flight_id,style,threshold_1,threshold_2):
    '''input: - the dataframe for selected flight
              - head or body
              - the plot division
              - fontisze of the labels
              - flight_id
    plot the YPR data'''
    
    a=fontsize
    sns.set_style("whitegrid", {'axes.grid' : False})
    data=DATA.xs(flight_id,level='flight_i')
    error=ERROR.xs(flight_id,level='flight_i')
    sac_index=RES.xs(flight_id,level='flight_i')
    
    ##devide tra in number of subdivision wanted:
    frames=data.index.get_level_values('frame_i')
    first_non_nan=frames.min()
    length=frames.max()-first_non_nan
    crops_len=length/sub
    
    ## rename the data
    df_orient=data.zyx
    df_orient=df_orient.reset_index('blk_i',drop=True)
    
    ##convert each orientation into degrees 
    df_orient=df_orient.loc[:,['alpha_0','alpha_1','alpha_2']].apply(lambda v: np.rad2deg(np.arctan2(np.sin(v), np.cos(v))))                                                            
    smooth_yaw=df_orient.alpha_0
    smooth_pitch=df_orient.alpha_1
    smooth_roll=df_orient.alpha_2
    
    ##get the error
    error=error.zyx.reset_index('blk_i',drop=True)
    std_yaw=np.sqrt(np.rad2deg(np.rad2deg(error.alpha_0.alpha_0)))
    std_pitch=np.sqrt(np.rad2deg(np.rad2deg(error.alpha_1.alpha_1)))
    std_roll=np.sqrt(np.rad2deg(np.rad2deg(error.alpha_2.alpha_2)))

    list_starts=list()
    # get the frames to start and end subdivision
    for i in np.arange(0,sub):
        list_starts.append(i*crops_len+first_non_nan)
    f,axs=plt.subplots(sub,1,figsize=(15,10))

    for ax1,fmin,nb in zip (axs.ravel(),list_starts,np.arange(0,sub)):
        
        ##limitation of the subplots
        fmax=fmin+int(crops_len)
        fmin=int(fmin)
        fmax=int(fmax)
        
        ##plot the absolute of the yaw velocity
        velocities=VEL.xs(flight_id,level='flight_i')
        yaw_vel=velocities.zyx.dalpha_2
        yaw_vel=yaw_vel.reset_index('blk_i',drop=True)
        yaw_vel=yaw_vel*500
        yaw_vel=np.rad2deg(yaw_vel)
        
        ##axis parameters
        ax1.set_xlim(fmin/500,fmax/500)
        ax1.set_ylabel('angle [°]',fontsize=a)
        ax2=ax1.twinx()
        ax2.set_ylim(0,1500)
        ax2.spines['left'].set_linewidth(1)
        ax2.spines['left'].set_color('k')
        ax2.spines['bottom'].set_linewidth(1)
        ax2.spines['bottom'].set_color('k')
        ax2.spines['right'].set_linewidth(1)
        ax2.spines['right'].set_color('k')
        ax2.axhline(np.rad2deg(threshold_1)*500,xmin=0,xmax=1,color='b',linestyle='--')
        ax2.axhline(np.rad2deg(threshold_2)*500,xmin=0,xmax=1,color='r',linestyle='--')
        

        #plot the different orientations with colors:
        colors=[['purple','#d980fa'],['green','Lightgreen'],['blue','LightBlue']]

        for orientation,error,cols,lab in zip([smooth_yaw,smooth_roll,smooth_pitch],
                                              [std_yaw,std_roll,std_pitch],
                                              colors,['yaw','pitch','roll']):
            
            ## to not plot the nans
            orientation=orientation[fmin:fmax]
            orientation=orientation.reindex(np.arange(fmin,fmax+1)).fillna(np.nan)
            error=error[fmin:fmax]
            error=error.reindex(np.arange(fmin,fmax+1)).fillna(np.nan)
            #express index in seconds
            time=orientation.index/500

            #make plot ax1
            ax1.plot(time,orientation,color=cols[0],linewidth=1,label=lab)
            ax1.fill_between(time,orientation- error,
                            orientation + error,color=cols[1],
                             alpha=0.7,linewidth=0)

        ## plot velovity ax2
        yaw_vel=yaw_vel[fmin:fmax]
        yaw_vel=yaw_vel.reindex(np.arange(fmin,fmax+1)).fillna(np.nan)
        ax2.plot(time,np.abs(yaw_vel),color='k',linewidth=1,label='yaw_vel')
        ax2.set_ylabel('yaw velocitiy [°/s]',fontsize=a)
        
        ###legend on first subplot
        if nb==0:
            
            ax1.legend(ncol=4, loc=0, bbox_to_anchor=(1, 1.4),fontsize=11)
            ax2.legend(ncol=4, loc=0, bbox_to_anchor=(0.75, 1.4),fontsize=11)
            
        if nb==(sub-1):
            ax1.set_xlabel('time [s]',fontsize=a)
            
        trans1 = mtransforms.blended_transform_factory(ax1.transData, ax1.transAxes)

        for name,saccade in sac_index.groupby(['blk_i','saccade']):     
            
            if style=='head':
                ax1.fill_between([saccade.index[0][1]/500,saccade.index.max()[1]/500], 0, 1,
                                 facecolor='grey', alpha=0.3, transform=trans1,linewidth=0)
            if style=='body':
                ax1.fill_between([saccade.index[0][1]/500,saccade.index.max()[1]/500], 0, 1,
                                 facecolor='w', edgecolor='k', transform=trans1,linestyle='--',linewidth=0.8)

    ax1.grid(False)
    ax2.grid(False)  
    sns.despine(right=False,offset=5)
    
    names_for_publi={'05':1,'06':2,'07':3,'08':4,'11':5,'17':6}
    
    f.suptitle('YPR flight_{}'.format(names_for_publi[flight_id]))
    f.subplots_adjust(hspace=0.4)
    plt.show()

    return f

def time_line(x,y,z,lw,cmap):
    '''plot colored line'''
    from matplotlib.collections import LineCollection
    from matplotlib.colors import ListedColormap, BoundaryNorm
    import matplotlib as mpl
    import matplotlib.collections as mcoll
    points = np.array([x, y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)

    # Default colors equally spaced on [0,1]:
    if z is None:
        z = np.linspace(0.0, 1.0, len(x))

    # Special case if a single number:
    # to check for numerical input -- this is a hack
    if not hasattr(z, "__iter__"):
        z = np.array([z])

    z = np.asarray(z)
   
    norm=plt.Normalize(z.min(), z.max())
   
    lc = mcoll.LineCollection(segments, array=z, cmap=cmap, norm=norm,linewidth=lw)

    # Create the line collection object, setting the colormapping parameters.
    # Have to set the actual values used for colormapping separately.

    lc.set_linewidth(3)

    return lc


def lolliplot_lolliplot(ax,conditional_index,yxz,yaw,colors,alphas=[1,1],seg='head',step=8,ds_which='both',cmap='jet',lw=3):

    '''Returns a lolliplot representative of the yaw orientation during flight 
        input : - the axis
            - the index color coding of specific occurences to highligh (e.g saccades)
            - xyz: x ,y, z positions 
            - yaw: yaw orientation
            - colors : arrays with string of colors
            - step: downsampling 
            - ds_which : 'both' : both lollipops, conditional index and others, are downsampled
                        'non_conditional': if lollipop belonging to the conditional index: not downsampled 
            - cmap : for the time colored
            - lw : linewidth of the time colored position
            '''

    ## plot the head position  as a coninuous line
    x_all=yxz.x
    y_all=yxz.y
    time=yxz.index
    lc=time_line(x=x_all,y=y_all,z=time,cmap=cmap,lw=1)
    ax.add_collection(lc)
    yaw=yaw.iloc[::step]
    if not conditional_index is None :
        index_rest=[x for x in yxz.index if not x in conditional_index]
    else:
        index_rest=yxz.index
    
    ##extract the yaw orientation
    yaw_rest=yaw.loc[index_rest]#.loc[::step]
    if seg=='thorax':
        
        yaw = np.arctan2(np.sin(yaw), np.cos(yaw))
        ##quiver arguments to plot vectors
        u = -np.cos(yaw_rest)
        v = -np.sin(yaw_rest)
    if seg=='head':
        
        yaw = np.arctan2(np.sin(yaw), np.cos(yaw))
        ##quiver arguments to plot vectors
        u = np.cos(yaw_rest)
        v = np.sin(yaw_rest)
    
    x_ds=yxz.x[index_rest]#.iloc[::step]
    y_ds=yxz.y[index_rest]#.iloc[::step]
    
    ##make quiver 
    ax.quiver(x_ds,y_ds,-u,-v,color=colors[0],alpha=alphas[0],scale=8,headlength=0,headwidth=0)

    if not conditional_index is None :
    
        if ds_which=='both':

            x_cond=yxz.x[conditional_index]#.iloc[::step]
            y_cond=yxz.y[conditional_index]#.iloc[::step]
            yaw_cond=yaw.loc[conditional_index]#.iloc[::step]
                                     
            yaw_cond = np.arctan2(np.sin(yaw_cond), np.cos(yaw_cond))
            if seg=='head':
                u_cond = np.cos(yaw_cond)
                v_cond = np.sin(yaw_cond)
            if seg=='thorax':
                u_cond = -np.cos(yaw_cond)
                v_cond = -np.sin(yaw_cond)
                
            ax.quiver(x_cond,y_cond,-u_cond,-v_cond,color=colors[1],alpha=alphas[1],scale=8,headlength=0,headwidth=0)
            
        if ds_which=='non_conditional':
                                     
            x_cond=yxz.x[conditional_index]
            y_cond=yxz.y[conditional_index]
            yaw_cond = yaw.loc[conditional_index]
            yaw_cond = np.arctan2(np.sin(yaw_cond), np.cos(yaw_cond))
            if seg=='head':
                u_cond = np.cos(yaw_cond)
                v_cond = np.sin(yaw_cond)
            if seg=='thorax':
                u_cond = -np.cos(yaw_cond)
                v_cond = -np.sin(yaw_cond)
         
            ax.quiver(x_cond,y_cond,-u_cond,-v_cond,color=colors[1],alphas=alphas[1],scale=8,headlength=0,headwidth=0)


    return lc
