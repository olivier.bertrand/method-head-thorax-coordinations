# Head and thorax orientation during bumblebees' learning flights

Briefly, bees were individually marked with six markers (three on their head, and three on their thorax), and their flights, when exiting their nest, recorded with three calibrated cameras. The timecourse of the position and orientation of head and thorax of bumblebees, bombus terrestris, during learning flights was obtained from the six markers. 

The requirement.txt contain a list of python packages used for the analyses.

![](setupbees.png)

## Chapter 1 | From markers to orientation and angular velocity

In the first chapter, the orientation of the bumblebees head and thorax is derived from the markers positions. Then the angular velocity and the segmentation of the flight into saccade and intersaccade is perfomed. The recording error is propagated through this process.

## Chapter 2 | The critical role of head movement for spatial representation

We look at the head movement during the learning flight of bumblebees and its impact on the optical flow. We study whether the optic flow carries information about distance in a allocentric or egocentric reference frame.  


## Chapter 3 | Segmentating flight from thorax angular velocity

We propose two methods (Decision Tree, and Random Forest) to segment the flight of bumblebees into saccade and intersaccade from the thorax angular velocity. The performance of our method is compared against a benchmark and tested in two often encountered situation.

## Chapter 4 | Predicting body part motion

We propose a method to study the causal link in the control of two body parts. We illustrate our method based on the head-thorax motion during learning flights of bumblebees.

Note: The method is computationally extensive. It was run on a 64 cores / 512Gb memory computing unit. Thus, some figures are produced from precomputed results. In the notebooks, computing time for desktop computer is indicated.

## References:
Doussot et al 2020 doi:10.3389/fnbeh.2020.606590
Odenthal et al 2020 doi:10.3389/fnbeh.2020.610029